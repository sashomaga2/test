import { ElementRef, Directive, Input, OnInit, EventEmitter, Output, ViewContainerRef, TemplateRef } from '@angular/core';
import { MatFormFieldModule, MatSelectModule, MatSelect,  } from '@angular/material';

@Directive({
  selector: '[AppDatePicker]'
})
export class DatePickerDirective implements OnInit {
  
  private _value : any;
  private element: HTMLInputElement;

  // @Input() set value(value) {
  //   debugger;
  //    this._value = value;
     
  // } 
  //@Output() dateChange = new EventEmitter();


  constructor(private elRef: ElementRef, 
              private container: ViewContainerRef, 
              private matSelect: MatSelectModule) { 
    this.element = elRef.nativeElement;
  }

  ngOnInit(): void {
    console.log("directive init");
    
    //this.matSelect

    //this.container.createEmbeddedView(this.template);
    
    //throw new Error("Method not implemented.");
  }

}
