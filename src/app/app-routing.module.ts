import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModalContainerComponent } from './modal-container/modal-container.component';
import { EmptyComponent } from './empty/empty.component'

const routes: Routes = [
  { path: 'account-edit/:id', component: ModalContainerComponent },
  { path: '', component: EmptyComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
