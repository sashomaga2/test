import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MockHttpClient } from './mock-http-client'
import { Account } from './account';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  constructor(private http: MockHttpClient) {
  }

  getAccounts(): Observable<Account[]> {
    return this.http.get('/api/accounts');
  }

  getAccount(id : number) : Observable<Account[]> {
    return this.http.get(`/api/accounts/${id}`);
  }

  updateAccount(account: Account): Observable<Account> {
    return this.http.put(`/api/accounts/${account.id}`, account);
  }
}
