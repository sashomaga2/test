import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Account } from './account';


const hardCodedAccounts: object[] = [
  { dateOfBirth: new Date(2018, 2, 1), fullName: "Todor Aleksandrov"},
  { dateOfBirth: new Date(2017, 2, 1), fullName: "Gosho Hubaveca"},
  { dateOfBirth: new Date(2010, 2, 10), fullName: "Tomi Dgeri"}
]

@Injectable({
  providedIn: 'root'
})
export class MockHttpClient {
  private _accounts: object[];

  constructor() { 
    this._accounts = hardCodedAccounts;
  }

  // TODO implement case -> this.http.get(`/api/accounts/${id}`)
  get(url : string) : Observable<Account[]> {
    return Observable.create(o => 
      { 
        o.next(this._accounts.map((element, index) => { 
          return { id: index, ...element }
        })); 
        o.complete(); 
      });
  }

  put(url: string, account: Account) : Observable<Account>{
    const { id, ...updatedAccount } = account;
    this._accounts[id] = updatedAccount;

    return Observable.create({ id, ...this._accounts[id] });
  }
}
