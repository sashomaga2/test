import { Component, AfterContentInit, Input, EventEmitter, Output } from '@angular/core';
import { Account } from '../account'; 
import { AccountService } from '../account.service';

@Component({
  selector: 'app-account-edit',
  templateUrl: './account-edit.component.html',
  styleUrls: ['./account-edit.component.css']
})
export class AccountEditComponent {

  @Input() set id(value : number) {
     this._id = value;
     this.accountService.getAccount(this.id).subscribe(
      response => {
        this.account = response[value]
        //debugger;
      },
      error => console.log('error', error)
    );
  }

  @Output() onClose = new EventEmitter();

  private _id: number;
  public account : Account;

  constructor(private accountService: AccountService ) {

  }

  update() {
    this.accountService.updateAccount(this.account);
    this.close();
  }

  close() {
    this.onClose.emit();
  }

}
