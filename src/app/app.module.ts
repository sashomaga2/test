import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule, MatSelectModule } from '@angular/material';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccountsComponent } from './accounts/accounts.component';
import { AccountEditComponent } from './account-edit/account-edit.component';
import { ModalContainerComponent } from './modal-container/modal-container.component';
import { EmptyComponent } from './empty/empty.component';
import { DatePickerDirective } from './date-picker.directive';
import { BirthDateComponent } from './birth-date/birth-date.component';

@NgModule({
  declarations: [
    AppComponent,
    AccountsComponent,
    AccountEditComponent,
    ModalContainerComponent,
    EmptyComponent,
    DatePickerDirective,
    BirthDateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSelectModule,MatFormFieldModule,
    ModalModule.forRoot(),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [AccountEditComponent],
})
export class AppModule { }
