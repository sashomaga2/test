import {Component, Input, OnInit, OnChanges} from "@angular/core";
//import {CORE_DIRECTIVES} from "@angular/common";
import {AbstractValueAccessor, MakeProvider} from "../abstract-value-accessor";


@Component({
  selector: 'app-birth-date',
  templateUrl: './birth-date.component.html',
  styleUrls: ['./birth-date.component.css'],
  //directives: [CORE_DIRECTIVES],
  providers: [MakeProvider(BirthDateComponent)]
})
export class BirthDateComponent extends AbstractValueAccessor implements OnInit, OnChanges {
  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    debugger;
  }
  
  years : number[] = [1999,2000];
  months: number[] = [1,2,3];
  days: number[] = [12,13,14];

  @Input('displaytext') displaytext: string;
  @Input('placeholder') placeholder: string;

  ngOnInit() {
     debugger;
  }

}
