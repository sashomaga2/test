import { Component, OnDestroy } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AccountEditComponent } from '../account-edit/account-edit.component';

@Component({
  selector: 'app-modal-container',
  template: ''
})
export class ModalContainerComponent implements OnDestroy {
  destroy = new Subject<any>();
  currentDialog: BsModalRef = null;

  constructor(
    private modalService: BsModalService,
    route: ActivatedRoute,
    private router: Router
  ) {
    var self = this;
    route.params.pipe(takeUntil(this.destroy)).subscribe(params => {
        self.currentDialog = self.modalService.show(AccountEditComponent);
        self.currentDialog.content.id = params.id;
        self.currentDialog.content.onClose.subscribe(() => self.close())
    });
  }

  ngOnDestroy() {
    this.destroy.next();
  }

  close() {
    this.router.navigateByUrl('/');
    // TODO fix does not work !
    this.modalService.hide(0);
  }
}
