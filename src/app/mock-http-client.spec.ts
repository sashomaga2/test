import { TestBed } from '@angular/core/testing';

import { MockHttpClient } from './mock-http-client';

describe('MockHttpClientService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MockHttpClient = TestBed.get(MockHttpClient);
    expect(service).toBeTruthy();
  });
});
